{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.modules.desktop.htop;
in
{
  options.modules.desktop.htop = {
    enable = lib.my.mkBoolOpt false;
  };
  config = lib.mkIf cfg.enable {
    home-manager.users.${config.user.name} =
      { config, ... }:
      {
        programs.htop = {
          enable = true;
          package = pkgs.htop-vim;
          settings =
            {
              hide_userland_threads = true;
              highlight_base_name = true;
              vim_mode = true;
              fields = with config.lib.htop.fields; [
                PID
                USER
                M_RESIDENT
                M_SHARE
                STATE
                PERCENT_CPU
                PERCENT_MEM
                IO_RATE
                TIME
                COMM
              ];
            }
            // (
              with config.lib.htop;
              leftMeters [
                (bar "LeftCPUs2")
                (bar "CPU")
                (bar "Memory")
                (bar "Swap")
              ]
            )
            // (
              with config.lib.htop;
              rightMeters [
                (bar "RightCPUs2")
                (text "Tasks")
                (text "LoadAverage")
                (text "Uptime")
              ]
            );
        };
      };
  };
}
